import React, { useState} from 'react'
import ReactDOM from 'react-dom'
import Button from "./Components/Button";

const App = (props) => {
    const [selected, setSelected] = useState(0)
    const [votaciones,setVotaciones] = useState([0])
    const [mostVotes, setMostVotes] = useState(-1)

    const [contador, setContador] = useState(0)

    const handleNext = () => {
        //SE ASIGNA EL INDICE SELECCIONADO
        setSelected(getRandomInt(0,anecdotes.length))
        calcularMostVotes()
    }

    function calcularMostVotes(){
        //SE DETERMINA EL INDICE DE LA ANECDOTA MAS VOTADA
        console.log(votaciones)

        let index=0
        let mayor=0
        while (index < anecdotes.length) {
            if(votaciones[index]){
                if(votaciones[index]>mayor){
                    mayor=votaciones[index]
                    setMostVotes(index)
                }
            }
            index++
        }

    }

    const handleVotacion = () => {
        //SE INCREMENTA LA VOTACIÓN DE ACUERDO A LA ANECDOTA SELECCIONADA
        if(!votaciones[selected]){
            votaciones[selected]=0
        }
        votaciones[selected]=votaciones[selected]+1
        setVotaciones(votaciones)
        setContador(contador+1)

        calcularMostVotes()
    }

    function getRandomInt(min, max) {
        //SE OBTIENE UN NUMERO ALEATORIO ENTRE UN RANGO
        return Math.floor(Math.random() * (max - min)) + min;
    }


    return (
        <div>
            <h1>Anecdote of the day</h1>
            {props.anecdotes[selected]}
            <br/>


            <p style={{color:"#C0392B"}}>has {votaciones[selected]} votes</p>
            <br/>

            <Button color="#2ECC71" tamaño="25px" title="Vote" handleClick={handleVotacion} />
            <Button color="#F1C40F" tamaño="25px" title="Next Anecdote" handleClick={handleNext} />

            <br/>

            {
                (mostVotes>=0) ?
                    <div>
                        <h1 >Anecdote with most votes</h1>
                        {props.anecdotes[mostVotes]}
                        <br/>
                         <p style={{color:"#C0392B"}}>has {votaciones[mostVotes]} votes</p>
                    </div>
                    :
                    <div></div>
            }



        </div>
    )

}

const anecdotes = [
    'If it hurts, do it more often',
    'Adding manpower to a late software project makes it later!',
    'The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
    'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
    'Premature optimization is the root of all evil.',
    'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.'
]





ReactDOM.render(
    <App anecdotes={anecdotes}  />,
    document.getElementById('root')
)
