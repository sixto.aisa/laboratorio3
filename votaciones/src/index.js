import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import Statistics from "./components/Statistics";
import Button from "./components/Button";

const App = () => {
    // save clicks of each button to its own state
    const [good, setGood] = useState(0)
    const [neutral, setNeutral] = useState(0)
    const [bad, setBad] = useState(0)

    const [puntuacion, setPuntuacion] = useState(0)

    const handleGood = () => {
        setGood(good+1)
        setPuntuacion(puntuacion+1)
    }

    const handleNeutral = () => {
        setNeutral(neutral+1)
        setPuntuacion(puntuacion+0)
    }

    const handleBad = () => {
        setBad(bad+1)
        setPuntuacion(puntuacion-1)
    }

    return (
        <div>

            <h1>give fedback</h1>


            <Button color="#2ECC71" tamaño="25px" title="good" handleClick={handleGood} />
            <Button color="#F1C40F" tamaño="25px" title="neutral" handleClick={handleNeutral} />
            <Button color="#EC7063" tamaño="25px" title="bad" handleClick={handleBad} />

            <hr/>

            <Statistics good={good} neutral={neutral} bad={bad} puntuacion={puntuacion} />



        </div>
    )
}

ReactDOM.render(<App />,
    document.getElementById('root')
)
