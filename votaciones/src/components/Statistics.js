import Statistic from "./Statistic";
import Button from "./Button";
import React from "react";

const Statistics=(props) =>{
    return (
        <div>
            <h1>Statistics</h1>
            {
                (props.good+props.neutral+props.bad)==0 ?
                <p>No feedback given </p>
            :
                <div>

                    <Statistic color="#2ECC71" tamaño="25px" text="good" value={props.good} />
                    <Statistic color="#F1C40F" tamaño="25px" text="neutral" value={props.neutral} />
                    <Statistic color="#EC7063" tamaño="25px" text="bad" value={props.bad} />
                    <Statistic color="#000" tamaño="25px" text="all" value={props.good + props.neutral + props.bad} />
                    <Statistic color="#000" tamaño="25px"text="average" value={props.puntuacion / (props.good + props.neutral + props.bad)} />
                    <Statistic color="#000" tamaño="25px" text="positive" value={(props.good / (props.good + props.neutral + props.bad)) * 100} />

                </div>

            }





        </div>
    )
}

export default Statistics
